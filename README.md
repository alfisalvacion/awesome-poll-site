### Polls.com
# An awesome polling app made for everyone who wants to know all things
Made with Python-Django, requirejs, mustachejs, and lessjs.
# Screenshots
### Login
![awesome_poll_site](screenshots/login.png)
### Signup
![awesome_poll_site](screenshots/signup.png)
### Home
![awesome_poll_site](screenshots/home.png)
### Add a Poll
![awesome_poll_site](screenshots/addpoll.png)