from django.conf.urls import url

from registration import views

app_name = 'registration'

urlpatterns = [
    url(r'^login/$', views.LogInView.as_view(), name='log_in'),
    url(r'^signup/$', views.SignUpView.as_view(), name='sign_up'),
    url(r'^editprofile/$', views.EditProfileView.as_view(), name='edit_profile'),
    url(r'^logout/$', views.LogOutView.as_view(), name='log_out')
]