# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.conf import settings
from django.views import generic
from django.views.generic import View
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin

from models import User
from models import Person


class LogInView(View):
    template_name = 'registration/login.html'

    def get(self, request):
        if request.user.is_authenticated:   
            return redirect('home')
        context = {}
        return render(request, self.template_name, context=context)

    def post(self, request):
        context = {}
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)

        if user:
            login(request, user)
            return HttpResponseRedirect(reverse('home'))
        else:
            context['error'] = 'Invalid Username or Password'
            context['username'] = username

        return render(request, 'registration/login.html', context=context)


class EditProfileView(LoginRequiredMixin, View):
    login_url = 'registration:log_in'
    redirect_field_name = 'redirect_to'
    
    template_name = 'registration/edit_profile.html'

    def get(self, request):
        if not request.user.is_authenticated:   
            return redirect('')
        return render(request, self.template_name, )

    def post(self, request):
        new_first_name = request.POST.get('first_name')
        new_last_name = request.POST.get('last_name')
        user = request.user
       
        if new_first_name != '':
            user.first_name = new_first_name

        if new_last_name != '':
            user.last_name = new_last_name
            user.save()

        # user_avatar = request.FILES.get('avatar')
        # print user_avatar

        # if user_avatar != None:
        #    fs = FileSystemStorage()
        #    user_avatar.name = 'user_' + str(request.user) + '_' + user_avatar.name
        #    filename = fs.save(user_avatar.name, user_avatar)

        #    person = Person.objects.get(id = request.user.id)
        #    person.avatar = filename
        #    person.save()

        return redirect('home')


class SignUpView(View):
    template_name = 'registration/signup.html'
    data = {}

    def get(self, request):
        if request.user.is_authenticated:
            return redirect('home')
        return render(request, self.template_name, )

    def post(self, request):
        self.data['username'] = request.POST.get('username')
        self.data['email'] = request.POST.get('email')
        self.data['password1'] = request.POST.get('password1')
        self.data['password2'] = request.POST.get('password2')

        print self.data

        dataCheck = self.user_integrityCheck(self.data)

        if dataCheck == True:
            created = User.objects.create_user(
                username = self.data['username'], 
                password = self.data['password1'],
                email = self.data['email'])
            
            created.save()

            user = authenticate(username = self.data['username'], password = self.data['password1'])
            login(request, user)

            return HttpResponseRedirect(reverse('home'))

        else:
            context = dataCheck[1]
            return render(request, 'registration/signup.html', context=context)

    def user_integrityCheck(self, data):
        errors = {}

        if len(User.objects.filter(username = data.get('username'))) > 0:
            errors['username_error'] = "Username is already taken."

            return False, errors

        if len(User.objects.filter(email = data.get('email'))) > 0:
            errors['email_error'] = "Email is already taken."

            return False, errors

        if data.get('password1') != data.get('password2'):
            errors['password_error'] = "Passwords do not match"

            return False, errors
            
        return True


class LogOutView(View):

    def get(self, request):
        logout(request)
        return HttpResponseRedirect(reverse('registration:log_in'))