from django.conf.urls import url
from django.contrib import admin

from . import views

app_name = 'polls'

urlpatterns = [
    # ../polls/5/results/
    url(r'^(?P<pk>[0-9]+)/results/$', views.ResultsView.as_view(), name="results"),
    # ../polls/5/vote/
    url(r'^(?P<question_id>[0-9]+)/vote/$', views.VoteView.as_view(), name="vote"),
    # ../polls/addquestion/
    url(r'^addpoll/$', views.AddPollView.as_view(), name="add_poll"),
    url(r'^trymustache/$', views.TryMustacheView.as_view(), name="try_mustache"),
    url(r'^mainmustache/$', views.MainMustacheView.as_view(), name="main_mustache")
]
