# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json

from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from django.core import serializers

from .models import Question, Choice, PollVote


class ResultsView(LoginRequiredMixin, generic.DetailView):
    login_url = 'registration:log_in'
    redirect_field_name = 'redirect_to'
    
    model = Question
    template_name = 'polls/results.html'


class VoteView(LoginRequiredMixin, generic.TemplateView):
    login_url = 'registration:log_in'
    redirect_field_name = 'redirect_to'

    def post(self, request, question_id, *args, **kwargs):
        question = get_object_or_404(Question, pk=question_id)
    
        poll_vote = PollVote.objects.filter(user_who_voted=request.user, question_which_is_voted=question)            
        
        if poll_vote.exists():
            return render(request, 'polls/results.html', {
                'question' : question,
                'error_message' : "You already voted here."
            })

        try:
            selected_choice = question.choice_set.get(pk=request.POST['choice'])
            
        except (KeyError, Choice.DoesNotExist):
            return render(request, 'polls/results.html', {
                'question' : question,
                'error_message' : "You didn't select a choice."
            })

        else:
            selected_choice.votes += 1
            selected_choice.save()

            PollVote.objects.create(user_who_voted=request.user, question_which_is_voted=question)

        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))


class AddPollView(LoginRequiredMixin, generic.TemplateView):
    login_url = 'registration:log_in'
    redirect_field_name = 'redirect_to'

    template_name = 'polls/addpoll.html'

    def post(self, request, *args, **kwargs):
        question_text = request.POST.get('question_text')
        question = Question.objects.create(question_text=question_text, pub_date=timezone.now(), user_who_asked=request.user)

        i = 0
        while request.POST.get('item' + str(i)) != None:
            choice_text = request.POST.get('item' + str(i))
            self.add_choice(question.id, choice_text)
            i += 1

        return redirect(reverse('home', ))

    def add_choice(self, question_id, choice_text):
        try:
            question_to_add_choice = Question.objects.get(id=question_id)

        except (KeyError, Question.DoesNotExist):
            return redirect(reverse('polls:index'), {
                'error_message' : "Error in adding a choice"
            })

        else:
            Choice.objects.create(question=question_to_add_choice, choice_text=choice_text, votes=0)


class TryMustacheView(generic.View):
    def post(self, request, *args, **kwargs):
        questions = serializers.serialize('json', Question.objects.all())
        print questions
        # questions = Question.objects.all()
        # q = {
            # 'questions' : questions
        # }
        return JsonResponse({
            'questions' : questions
        })


    def get(self, request):
        questions = serializers.serialize('json', Question.objects.all())
        print questions
        context = {
            'questions' : questions
        }

        return JsonResponse({
            'questions' : questions
        })

        # return HttpResponse(json.dumps(context), 200)


class MainMustacheView(generic.TemplateView):

    template_name = 'polls/trymustache.html'
