# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-08-28 10:34
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0004_pollvotes'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='PollVotes',
            new_name='PollVote',
        ),
    ]
