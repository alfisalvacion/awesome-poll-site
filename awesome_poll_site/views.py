# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views import generic
from django.utils import timezone

from polls import views
from polls.models import Question

class IndexView(LoginRequiredMixin, generic.ListView):
    login_url = 'registration:log_in'
    redirect_field_name = 'redirect_to'

    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        return Question.objects.filter(
            pub_date__lte=timezone.now(),
        ).order_by('-pub_date')[:10]